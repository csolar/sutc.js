# Sweep Under The Carpet (sutc.js)

sutc.js is an anti-pattern for educational purposes that should never be needed if you follow good programming practices. However we use it here to illustrate *reflection* in javascript. Nonetheless, sometimes a quick and dirty patch may be needed when refactoring is out of the question due to time constraints and risk of introducing bugs: that's when you might use something like this...don't tell anyone though...

## A use case  

You are working on a large project and you are using some kind on templating engine. 
You use partial templates everywhere and you move all that is common in your screens (e.g. header, footer) to some base template(s) that gets inherited from all others. 
You also use javascript libraries like JQuery that allow on the fly extension of the global object's prototype in order to provide useful plugins.

Normally a base template would load only the plugins that all its extensions will need and defer loading of specific plugins to the specific templates that need them. A template should never attempt to reference a plugin that wasn't previously loaded/installed somewhere. 

However as it happens, a base template may end up with code that references plugins that only a few extending templates actually load with the consequence that if the plugins is not loaded when rendering a page, javascript will throw an error and the DOM will break.

## Sweep it under the carpet

sutc.js allows you to sweep these errors under the carpet so that the DOM will load no matter what. It does so in the least intrusive way in the sense that you only have to write one line of code. Assuming sotc.js is loaded all you have to do is to make sure you turn the target object (e.g. jQuery) into a sutc Proxy, for example:

	:::javascript
	(function (window, document, jQuery, Sutc){
		"use strict"

		jQuery = Sutc.proxy(jQuery);

		// do as you please with jQuery 
		...
	})(window, document, jQuery, Sutc) 

In order to accomplish this sutc.js uses javascript reflection tools, specifically `Proxy` and `Reflect` (see: https://goo.gl/8QxK7X). Any safe calls are dispatched to the original `jQuery` object whereas unsafe calls are proxied by dummy functions that do nothing.