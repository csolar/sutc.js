var Sutc = (function(){
	"use strict";

	function Sutc(){}

	Sutc.prototype = {
		proxy: function (subject) {
			// this object handles the trap that 
			// javascript fires when a property 
			// is called on our proxy object
			var handler = {
				// let's trap calls to properties on our target
				get: function (target, name, receiver) {
					if (name in target && (typeof target[name] != 'function')) {
						// if we are here the requested
						// property exists and  is not a function
						// so we simply return its value
						return target[name]
					}
					if (name in target.__proto__) {
						// if we are here our original object
						// actually has the requested property
						// and it's a function so we defer 
						// execution to it
						return function (...args) {
							return target[name].apply(target, args);
						}
					}
					else {
						// if we are here our object does not
						// have the requested property so we
						// sweep the error under the carpet and
						// simply return a dummy function that
						// does nothing thus preventing Javascript
						// from issuing an error
						return function (...args) {
							return
						}
					}
				}
			}

			// here we return the proxy to our target object
			// however we make sure that we also trap calls to non-prototype
			// functions so that we may return a safe proxy when this happens
			// check Proxy and Reflect documentation here: https://goo.gl/8QxK7X
			return new Proxy (subject, {
				apply: function (target, receiver, args) {
					return new Proxy (Reflect.apply(target, receiver, args), handler)
				},
				handler
			});
		}
	}

	return new Sutc();
})();