(function (globals) {
	"use strict";

	var expect = globals.chai.expect,
		Sutc = globals.Sutc,
		jQuery = globals.jQuery,
		badFun = function () {
			jQuery('body').SomeNonExistentPlugin();
		};

	describe('sutc test', function () {
		it ('should raise a TypeError if jQuery is not sutc-proxied', function () {
			expect(badFun).to.throw(TypeError);
		});

		it ('should not raise a TypeError if jQuery is sutc-proxied', function () {
			jQuery = Sutc.proxy(jQuery);

			expect(badFun).to.not.throw(TypeError);
		});
	})
})(this)